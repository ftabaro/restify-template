import Handlers from './handlers'

export default {
  'POST': {},
  'GET': {
    'root': {
      path: '/',
      handler: Handlers.handleRoot
    }
  },
};